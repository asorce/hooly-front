import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from "./auth/auth.module";
import { BookingsModule } from "./bookings/bookings.module";
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from "./core/core.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { SlotsModule } from "./slots/slots.module";
import { registerLocaleData } from "@angular/common";
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    AuthModule,
    BrowserModule,
    CoreModule,
    SlotsModule,
    BookingsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [AppRoutingModule, ReactiveFormsModule, FormsModule],
  providers: [{
    provide: LOCALE_ID, useValue: 'fr-FR',
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
