import {BookingDto} from "../booking/booking.dto";

export interface SlotDto {
  date: string,
  numberOfSlot: number,
  bookings: BookingDto[];
  past: boolean;
}
