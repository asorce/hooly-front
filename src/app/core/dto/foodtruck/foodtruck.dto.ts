export interface FoodtruckDto {
  id: number;
  name: string;
  email: string;
  token: string;
}
