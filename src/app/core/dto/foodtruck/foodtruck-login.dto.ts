export interface FoodtruckLoginDto {
  username: string;
  password: string;
}
