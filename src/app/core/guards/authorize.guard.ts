import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {ApiService} from "../services/api.service";
import {LocalStorageService} from "../services/local-storage.service";


@Injectable({
  providedIn: 'root'
})
export class AuthorizeGuard implements CanActivate {
  constructor(
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private router: Router
  ) { }

  async canActivate(
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot
  ): Promise<boolean> {
    // Simplified version of a JWT where we don't check the expiresAt
    if (this.localStorage.getToken()) {
      if (this.localStorage.getToken() === '') {
        this.localStorage.remove('token');
        this.localStorage.remove('foodtruck');
        return this.router.navigateByUrl('/login');
      } else {
        return true;
      }
    } else {
      return this.router.navigateByUrl('/login');
    }
  }
}
