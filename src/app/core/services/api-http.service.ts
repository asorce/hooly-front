import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LocalStorageService} from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpService {

  constructor(private http: HttpClient) { }

  public getRaw(url: string, options?: any): Observable<any> {
    return this.http.get(url, options);
  }
  public get(url: string, options?: any): Observable<any> {
    if (!options) {
      options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Authorization',
          'Access-Control-Allow-Credentials' : 'true',
          'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        })
      };
    }
    return this.http.get(url, options);
  }
  public post(url: string, data: any, options?: any): any {
    return this.http.post(url, data, options);
  }
  public patch(url: string, data: any, options?: any): any {
    return this.http.patch(url, data, options);
  }
  public put(url: string, data: any, options?: any): any {
    return this.http.put(url, data, options);
  }
  public delete(url: string, options?: any): any {
    if (!options) {
      options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Authorization',
          'Access-Control-Allow-Credentials' : 'true',
          'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        })
      };
    }
    return this.http.delete(url, options);
  }
}
