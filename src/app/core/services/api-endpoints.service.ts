import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UrlBuilder } from '../utils/url-builder';

@Injectable({
  providedIn: 'root'
})
export class ApiEndpointsService {
  constructor() { }

  private createUrl(
    action: string
  ): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      environment.API_ENDPOINT,
      action
    );
    return urlBuilder.toString();
  }

  public postLoginEndpoint(): string {
    return this.createUrl('login');
  }

  public getSlotsEndpoint(): string {
    return this.createUrl('slots');
  }

  public getSlotsWeekYearEndpoint(week: number, year: number): string {
    return this.createUrl('slots/' + week + '/' + year);
  }

  public getBookingsEndpoint(): string {
    return this.createUrl('bookings');
  }

  public createBookingEndpoint(date: string): string {
    return this.createUrl('booking/' + date);
  }

  public deleteBookingEndpoint(id: number): string {
    return this.createUrl('booking/' + id);
  }
}
