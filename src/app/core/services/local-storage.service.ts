import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  set(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  get(key: string): string {
    const item = localStorage.getItem(key);
    return item || '';
  }

  remove(key: string): void {
    localStorage.removeItem(key);
  }

  clear(): void {
    this.remove('foodtruck');
    this.remove('token');
  }
  getToken(): string {
    return this.get('token') ?? '';
  }
}
