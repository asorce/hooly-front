import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiEndpointsService } from './api-endpoints.service';
import { ApiHttpService } from './api-http.service';
import { LocalStorageService } from "./local-storage.service";
import { BookingDto } from "../dto/booking/booking.dto";
import { FoodtruckDto } from "../dto/foodtruck/foodtruck.dto";
import { FoodtruckLoginDto } from "../dto/foodtruck/foodtruck-login.dto";
import { SlotDto } from "../dto/slot/slot.dto";

/**
 * The logic to make API calls is :
 *  -> getting the url from the environment
 *  -> getting the endPoint
 *  -> building a URL
 *  -> building headers for the specific method
 *  -> building a prepared call GET / POST ... for the specific method
 *    -> the caller has to subscribe to it to start the API call
 *
 * The auth is not with a JWT - I thought it was an overkill for this project -
 * -> just a regular random string that has an expiresAt in the backend
 * -> the token is passed in the header
 *    (like a session cookie but the token illustrates the concept of JWT)
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private apiHttpService: ApiHttpService,
              private apiEndpointsService: ApiEndpointsService,
              private localStorageService: LocalStorageService
  ) {}

  login(foodtruckLogin: FoodtruckLoginDto): Observable<FoodtruckDto> {
    return this.apiHttpService.post(this.apiEndpointsService.postLoginEndpoint(), foodtruckLogin);
  }

  listSlots(): Observable<SlotDto[]> {
    const httpOptions = this.getHeadersToken();
    return this.apiHttpService.get(this.apiEndpointsService.getSlotsEndpoint(), httpOptions);
  }

  listSlotsWeekYear(week: number, year: number): Observable<SlotDto[]> {
    const httpOptions = this.getHeadersToken();
    return this.apiHttpService.get(this.apiEndpointsService.getSlotsWeekYearEndpoint(week, year), httpOptions);
  }

  listBookings(): Observable<BookingDto[]> {
    const httpOptions = this.getHeadersToken();
    return this.apiHttpService.get(this.apiEndpointsService.getBookingsEndpoint(), httpOptions);
  }

  createBooking(date: string): Observable<boolean> {
    const httpOptions = this.getHeadersToken();
    return this.apiHttpService.post(this.apiEndpointsService.createBookingEndpoint(date), date, httpOptions);
  }

  deleteBooking(id: number): Observable<boolean> {
    const httpOptions = this.getHeadersToken();
    return this.apiHttpService.delete(this.apiEndpointsService.deleteBookingEndpoint(id), httpOptions);
  }

  private getHeadersToken(): Object {
    const token = this.localStorageService.get('token');
    return {
      headers: new HttpHeaders({
        'X-AUTH-TOKEN': token
      })
    };
  }
}

