import { NgModule } from '@angular/core';
import { AuthorizeGuard } from "./core/guards/authorize.guard";
import { RouterModule, Routes } from '@angular/router';
import { BookingsComponent } from "./bookings/bookings.component";
import { LoginComponent } from "./auth/login/login.component";
import { SlotsComponent } from "./slots/slots.component";

const routes: Routes = [
  { path: 'bookings', component: BookingsComponent, canActivate: [AuthorizeGuard] },
  { path: 'slots', component: SlotsComponent, canActivate: [AuthorizeGuard] },
  { path: 'login', component: LoginComponent },

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: LoginComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
