import { Component, OnInit } from '@angular/core';
import {ApiService} from "../core/services/api.service";
import {LocalStorageService} from "../core/services/local-storage.service";
import {Router} from "@angular/router";
import {BookingDto} from "../core/dto/booking/booking.dto";

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
  bookings: BookingDto[];
  loading: boolean;
  constructor(
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {
    this.bookings = [];
    this.loading = true;
    const token = this.localStorageService.get('token');
    if (!token || token === '') {
      this.localStorageService.clear();
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit(): void {
    this.reloadBookings();
  }
  reloadBookings(): void {
    this.apiService.listBookings().subscribe((bookings) => {
      this.processBookingsFromApi(bookings);
    })
  }

  processBookingsFromApi(bookings: BookingDto[]): void {
    this.bookings = bookings;
    this.loading = false;
  }

  deleteBooking(booking: BookingDto): void {
    this.loading = true;
    if (booking.id) {
      this.apiService.deleteBooking(booking.id).subscribe((deleted: boolean) => {
        this.reloadBookings();
      });
    }
  }

}
