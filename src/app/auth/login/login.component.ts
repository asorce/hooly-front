import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../core/services/api.service";
import {LocalStorageService} from "../../core/services/local-storage.service";
import {Router} from "@angular/router";
import {FoodtruckDto} from "../../core/dto/foodtruck/foodtruck.dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject, takeUntil} from "rxjs";
import {FoodtruckLoginDto} from "../../core/dto/foodtruck/foodtruck-login.dto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  foodtruckLoggedIn = {} as FoodtruckDto;
  foodtruckForm: FormGroup;
  loginError: boolean;
  notifierDestroySubscription = new Subject();
  constructor(
    private api: ApiService,
    private localStorage: LocalStorageService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.resetFormGroup();
  }
  resetFormGroup(): void {
    this.foodtruckForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
      ])
    });
  }

  login(): void {
    if (this.foodtruckForm) {
      const username = this.foodtruckForm.value.email;
      const password = this.foodtruckForm.value.password;
      const foodtruckLogin: FoodtruckLoginDto = {
        username,
        password
      };
      this.api.login(foodtruckLogin)
        .pipe(takeUntil(this.notifierDestroySubscription))
        .subscribe((foodtruck: FoodtruckDto) => {
          if (foodtruck) {
            const token = foodtruck.token;
            this.localStorage.set('token', token);
            this.localStorage.set('foodtruck', JSON.stringify(foodtruck));
            this.router.navigateByUrl('slots');
          }
        }, (error: any) => {
              this.loginError = true;
        });
    }
  }

  logout(): void {
    this.localStorage.remove('token');
    this.localStorage.remove('foodtruck');
    this.foodtruckLoggedIn = {} as FoodtruckDto;
  }

  ngOnDestroy(): void {
    // @ts-ignore
    this.notifierDestroySubscription.next();
    this.notifierDestroySubscription.complete();
  }
}
