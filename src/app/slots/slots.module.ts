import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlotsComponent } from './slots.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    SlotsComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SlotsModule { }
