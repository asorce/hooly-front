import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApiService} from "../core/services/api.service";
import {LocalStorageService} from "../core/services/local-storage.service";
import {BookingDto} from "../core/dto/booking/booking.dto";
import {SlotDto} from "../core/dto/slot/slot.dto";

@Component({
  selector: 'app-slots',
  templateUrl: './slots.component.html',
  styleUrls: ['./slots.component.scss']
})
export class SlotsComponent implements OnInit {
  slots: SlotDto[]
  hasBookingThisWeek: boolean;
  currentWeekStart: Date;
  currentWeekEnd: Date;
  currentWeekNumber: number;
  currentWeekYear: number;
  loading: boolean;
  constructor(
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {
    this.slots = [];
    this.hasBookingThisWeek = false;
  }

  ngOnInit(): void {
    this.apiService.listSlots().subscribe((slots) => {
      this.loading = true;
      const currentWeekNumber = this.localStorageService.get('currentWeekNumber');
      const currentWeekYear = this.localStorageService.get('currentWeekYear');
      if (currentWeekNumber && currentWeekNumber !== '' && currentWeekYear && currentWeekYear !== '') {
        this.currentWeekNumber = JSON.parse(currentWeekNumber);
        this.currentWeekYear = JSON.parse(currentWeekYear);
        this.reloadSlots();
      } else {
        this.processSlotsFromApi(slots);
      }
    }, error => {
      this.localStorageService.clear();
      this.router.navigateByUrl('/login');
    })
  }

  /**
   * Method that compute the slots from the API
   * -> dates are transformed to fill the week start and end
   * @param slots
   * @private
   */
  private processSlotsFromApi(slots: SlotDto[]): void {
    this.hasBookingThisWeek = false;
    this.checkHasBookingThisWeek(slots);
    this.checkIsDatePast(slots);
    if (slots && slots.length > 0) {
      const currentWeekStart = new Date(slots[0].date);
      const currentWeekEnd = new Date(slots[slots.length - 1].date);
      this.currentWeekStart = new Date(Date.UTC(currentWeekStart.getFullYear(), currentWeekStart.getMonth(), currentWeekStart.getDate()));
      this.currentWeekEnd = new Date(Date.UTC(currentWeekEnd.getFullYear(), currentWeekEnd.getMonth(), currentWeekEnd.getDate()));;
      const weekNumberFromDate = this.getWeekNumberFromDate(this.currentWeekStart);
      this.currentWeekYear = weekNumberFromDate[0];
      this.currentWeekNumber = weekNumberFromDate[1];
    }
    this.slots = slots;
    this.loading = false;
  }

  /**
   * The slots are just counted so in order to loop trough them
   * -> we need to create an array filled with these
   * @param slot
   */
  getNumberOfSlots(slot: SlotDto): number[] {
    const number = slot.numberOfSlot - slot.bookings.length;
    return [...Array(number).keys()];
  }

  getBookings(slot: SlotDto): number[] {
    const number = slot.bookings.length;
    return [...Array(number).keys()];
  }

  checkHasBookingThisWeek(slots: SlotDto[]): void {
    slots.forEach((slot: SlotDto) => {
      slot.bookings.forEach((booking: BookingDto) => {
        if (booking.id) {
          this.hasBookingThisWeek = true;
        }
      });
    })
  }

  checkIsDatePast(slots: SlotDto[]): SlotDto[] {
    return slots.map((slot: SlotDto) => {
      if (new Date(slot.date).valueOf() < new Date().valueOf()) {
        slot.past = true;
      }
      return slot;
    });
  }

  checkForbiddenSlot(slot: SlotDto): boolean {
    return slot.past || this.hasBookingThisWeek;
  }

  /**
   * Compute the year and week number for a given date
   * (thank you stackoverflow)
   * @param date
   */
   getWeekNumberFromDate(date: Date) {
    date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay()||7));
    const yearStart = new Date(Date.UTC(date.getUTCFullYear(),0,1));
    // @ts-ignore
     const weekNo = Math.ceil(( ( (date - yearStart) / 86400000) + 1) / 7);
    return [date.getUTCFullYear(), weekNo];
  }

  changeWeek(direction: number): void {
    this.loading = true;
    this.currentWeekNumber = this.currentWeekNumber + direction;
    if (this.currentWeekNumber > 52) {
      this.currentWeekNumber = 1;
      this.currentWeekYear = this.currentWeekYear + 1;
    }
    if (this.currentWeekNumber === 0) {
      this.currentWeekNumber = 1;
      this.currentWeekYear = this.currentWeekYear - 1;
    }
    this.localStorageService.set('currentWeekNumber', JSON.stringify(this.currentWeekNumber));
    this.localStorageService.set('currentWeekYear', JSON.stringify(this.currentWeekYear));
    this.reloadSlots();
  }

  reloadSlots(): void {
    this.apiService.listSlotsWeekYear(this.currentWeekNumber, this.currentWeekYear).subscribe((slots) => {
      this.processSlotsFromApi(slots);
    }, error => {
      // maybe retry the data or check the input - not managed
    })
  }

  askBooking(slot: SlotDto): void {
    if (!this.hasBookingThisWeek && !slot.past) {
      this.loading = true;
      this.apiService.createBooking(slot.date).subscribe((created: boolean) => {
        if (created) {
          this.reloadSlots();
        }
      })
    }
  }

  deleteBooking(booking: BookingDto): void {
    if (booking.id) {
      this.loading = true;
      this.apiService.deleteBooking(booking.id).subscribe((deleted: boolean) => {
        this.reloadSlots();
        this.hasBookingThisWeek = false;
      });
    }
  }
}
